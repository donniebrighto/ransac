package ransac;

import java.util.ArrayList;
import java.util.List;
import org.opencv.core.Mat;

public class Model {

  private List<KeyPoint[]> consensus;
  private Mat transformation;

  public Model(Mat transformation) {
    this.transformation = transformation;
    this.consensus = new ArrayList<>();
  }

  public List<KeyPoint[]> getConsensus() {
    return consensus;
  }

  public void addToConsensus(KeyPoint[] pair) {
    consensus.add(pair);
  }

  public Mat getTransformation() {
    return transformation;
  }
}
