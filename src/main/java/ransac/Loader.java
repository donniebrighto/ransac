package ransac;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Loader {

  public static Image loadImage(String filename) {
    try (BufferedReader reader =
        new BufferedReader(new FileReader("src/main/resources/" + filename))) {

      int numberOfFeatures = Integer.parseInt(reader.readLine());
      int numberOfPoints = Integer.parseInt(reader.readLine());

      KeyPoint[] keyPoints = parseCorePoints(reader, numberOfFeatures, numberOfPoints);

      return new Image(filename.split("\\.")[0], keyPoints);

    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  private static KeyPoint[] parseCorePoints(
      BufferedReader reader, int numberOfFeatures, int numberOfPoints) throws IOException {
    KeyPoint[] keyPoints = new KeyPoint[numberOfPoints];
    for (int i = 0; i < numberOfPoints; i++) {
      String line = reader.readLine();
      String[] splitted = line.split(" ");
      int[] features = parseFeatures(numberOfFeatures, splitted);
      keyPoints[i] =
          new KeyPoint(Float.parseFloat(splitted[0]), Float.parseFloat(splitted[1]), features);
    }
    return keyPoints;
  }

  private static int[] parseFeatures(int numberOfFeatures, String[] splitted) {
    int[] features = new int[numberOfFeatures];
    for (int j = 5, k = 0; k < numberOfFeatures; j++, k++) {
      features[k] = Integer.parseInt(splitted[j]);
    }
    return features;
  }
}
