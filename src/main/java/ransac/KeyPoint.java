package ransac;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;

public class KeyPoint extends Point {

  private int[] features;
  private KeyPoint closestNeighbour;
  private Image image;

  public KeyPoint(double xCoord, double yCoord, int[] features) {
    this.x = xCoord;
    this.y = yCoord;
    this.features = features;
  }

  public int featureDistance(KeyPoint other) {
    int[] otherFeatures = other.getFeatures();
    int distance = 0;
    for (int i = 0; i < features.length; i++) {
      distance += Math.abs(otherFeatures[i] - features[i]);
    }
    return distance;
  }

  public boolean hasMutualClosestNeighbour() {
    if (closestNeighbour == null) {
      return false;
    }
    return this.equals(closestNeighbour.getClosestNeighbour());
  }

  public KeyPoint getClosestNeighbour() {
    return closestNeighbour;
  }

  public void setClosestNeighbour(KeyPoint closestNeighbour) {
    this.closestNeighbour = closestNeighbour;
  }

  public int[] getFeatures() {
    return features;
  }

  public int getxCoord() {
    return (int) x;
  }

  public int getyCoord() {
    return (int) y;
  }

  public Image getImage() {
    return image;
  }

  public void setImage(Image image) {
    this.image = image;
  }

  public KeyPoint[] getClosestNeighbours(int maxNeighbourhood, List<KeyPoint[]> pairs, int index) {
    KeyPoint[] keyPoints =
        pairs.stream()
            .filter(pair -> !this.equals(pair[index]))
            .map(pair -> pair[index])
            .toArray(KeyPoint[]::new);
    Arrays.sort(keyPoints, Comparator.comparingDouble(this::distance));
    return topElementsOfArray(maxNeighbourhood, keyPoints);
  }

  private KeyPoint[] topElementsOfArray(int maxNeighbourhood, KeyPoint[] keyPoints) {
    KeyPoint[] closest = new KeyPoint[maxNeighbourhood];
    for (int i = 0; i < maxNeighbourhood; i++) {
      closest[i] = keyPoints[i];
    }
    return closest;
  }

  public double distance(KeyPoint other) {
    return Math.sqrt(Math.pow(other.x - x, 2) + Math.pow(other.y - y, 2));
  }

  public void setCoord(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public KeyPoint transform(Mat model) {
    Mat mat1 = new Mat(3, 1, CvType.CV_64F);
    mat1.put(0, 0, x);
    mat1.put(1, 0, y);
    mat1.put(2, 0, 1);

    Mat result = Mat.zeros(3, 1, CvType.CV_64F);
    result.put(3, 0, 1);
    Core.gemm(model, mat1, 1, new Mat(), 0, result, 0);

    double x = result.get(0, 0)[0];
    double y = result.get(1, 0)[0];
    System.out.println("coords: " + x + "," + y);
    return new KeyPoint(x, y, null);
  }
}
