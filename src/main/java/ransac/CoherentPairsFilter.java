package ransac;

import java.util.ArrayList;
import java.util.List;

public class CoherentPairsFilter {

  public static final int SEARCH_FROM_FIRST_MEMBER = 0;
  public static final int SEARCH_FROM_SECOND_MEMBER = 1;

  public List<KeyPoint[]> coherentPairs(
      int maxNeighbourhood, int threshold, List<KeyPoint[]> pairs) {
    List<KeyPoint[]> filteredPairs = new ArrayList<>();
    int averageCounter = 0;
    for (KeyPoint[] pair : pairs) {
      KeyPoint[] firstNeighbourhood = pair[0].getClosestNeighbours(maxNeighbourhood, pairs,
          SEARCH_FROM_FIRST_MEMBER);
      KeyPoint[] secondNeighbourhood = pair[1].getClosestNeighbours(maxNeighbourhood, pairs,
          SEARCH_FROM_SECOND_MEMBER);
      int neighbourPairs = countNeighbourPairs(firstNeighbourhood, secondNeighbourhood);
      averageCounter += neighbourPairs;
      if (neighbourPairs >= threshold) {
        filteredPairs.add(pair);
      }
    }
    System.out.println("Average: " + averageCounter/pairs.size());
    return filteredPairs;
  }

  private int countNeighbourPairs(KeyPoint[] firstNeighbourhood, KeyPoint[] secondNeighbourhood) {
    int count = 0;
    for (int i = 0; i < firstNeighbourhood.length; i++) {
      for (int j = 0; j < secondNeighbourhood.length; j++) {
        if (firstNeighbourhood[i].getClosestNeighbour().equals(secondNeighbourhood[j])) {
          count++;
          break;
        }
      }
    }
    return count;
  }
}
