package ransac;

import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.imgproc.Imgproc;
import ransac.heuristics.IterationsHeuristic;
import ransac.heuristics.SamplesDrawingHeuristic;

public class Ransac {

  public static final int FIRST_POINT = 0;
  public static final int SECOND_POINT = 1;

  private SamplesDrawingHeuristic samplesDrawingHeuristic;
  private IterationsHeuristic iterationsHeuristic;
  private double maxError;

  public Ransac(
      SamplesDrawingHeuristic samplesDrawingHeuristic,
      IterationsHeuristic iterationsHeuristic,
      double maxError) {
    this.samplesDrawingHeuristic = samplesDrawingHeuristic;
    this.iterationsHeuristic = iterationsHeuristic;
    this.maxError = maxError;
  }

  public Model estimateModel(List<KeyPoint[]> data, int samples) {
    long startTime = System.nanoTime();
    Model bestModel = null;
    int bestScore = -1;
    for (int i = 0; i < iterationsHeuristic.numberOfIterations(); i++) {
      List<KeyPoint[]> randomPairs = samplesDrawingHeuristic.drawPairs(data, samples);
      KeyPoint[] srcImage = randomPairs.stream().map(pair -> pair[0]).toArray(KeyPoint[]::new);
      KeyPoint[] destImage = randomPairs.stream().map(pair -> pair[1]).toArray(KeyPoint[]::new);

      Model model = new Model(calculateModel(srcImage, destImage, samples));
      int score = evaluateModel(maxError, data, model);

      if (score > bestScore) {
        bestModel = model;
        bestScore = score;
      }
    }
    System.out.println((System.nanoTime() - startTime)/1_000_000_000.0 + " [s]");
    return bestModel;
  }

  private int evaluateModel(double maxError, List<KeyPoint[]> data, Model model) {
    int score = 0;
    for (KeyPoint[] pair : data) {
      KeyPoint transformed = pointTransformed(model.getTransformation(), pair[FIRST_POINT]);
      if (pair[SECOND_POINT].distance(transformed) < maxError) {
        score++;
        model.addToConsensus(pair);
        samplesDrawingHeuristic.incrementCounter(pair);
      }
    }
    return score;
  }

  private KeyPoint pointTransformed(Mat model, KeyPoint keyPoint) {
    Mat mat1 = new Mat(3, 1, CvType.CV_64F);
    mat1.put(0, 0, keyPoint.getxCoord());
    mat1.put(1, 0, keyPoint.getyCoord());
    mat1.put(2, 0, 1);

    Mat result = Mat.zeros(3, 1, CvType.CV_64F);
    Core.gemm(model, mat1, 1, new Mat(), 0, result, 0);

    double x = result.get(0, 0)[0];
    double y = result.get(1, 0)[0];
    return new KeyPoint(x, y, null);
  }

  private Mat calculateModel(KeyPoint[] srcImage, KeyPoint[] destImage, int samples) {
    if (samples == 3) {
      return Imgproc.getAffineTransform(new MatOfPoint2f(srcImage), new MatOfPoint2f(destImage));
    } else {
      return Imgproc.getPerspectiveTransform(
          new MatOfPoint2f(srcImage), new MatOfPoint2f(destImage));
    }
  }
}
