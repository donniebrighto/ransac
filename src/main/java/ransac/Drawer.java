package ransac;

import java.util.List;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Drawer {

  private String firstImageName;
  private String secondImageName;
  private BufferedImage image;

  public Drawer(String firstImageName, String secondImageName) {
    this.firstImageName = firstImageName;
    this.secondImageName = secondImageName;
    this.image = initConcatImage();
  }

  public void draw(List<KeyPoint[]> keyPoints, Color color){
    Graphics2D graphics2D = image.createGraphics();
    graphics2D.setPaint(color);
    keyPoints.forEach(
        pair -> graphics2D.drawLine(pair[0].getxCoord(), pair[0].getyCoord(), pair[1].getxCoord(), pair[1].getyCoord() + 600)
    );
  }

  public BufferedImage initConcatImage(){
    BufferedImage firstImage = new BufferedImage(800, 1200, BufferedImage.TYPE_INT_RGB);
    BufferedImage secondImage = new BufferedImage(800, 1200, BufferedImage.TYPE_INT_RGB);
    BufferedImage concatImage = new BufferedImage(800, 1200, BufferedImage.TYPE_INT_RGB);

    String separator = System.getProperty("file.separator");
    try {
      firstImage = ImageIO.read(new File(
          "src" + separator + "main" + separator + "resources" + separator + firstImageName));
      secondImage = ImageIO.read(new File(
          "src" + separator + "main" + separator + "resources" + separator + secondImageName));
    } catch (IOException e) {
      e.printStackTrace();
    }

    Graphics2D graphics =  concatImage.createGraphics();
    graphics.drawImage(firstImage, 0, 0, null);
    graphics.drawImage(secondImage, 0, 600, null);

    return concatImage;
  }

  public void saveToFile(String filename) {
    String separator = System.getProperty("file.separator");
    try {
      ImageIO
          .write(image, "png", new File("src" + separator + "main" + separator + "resources" + separator + filename));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}