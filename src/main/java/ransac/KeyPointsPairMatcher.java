package ransac;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class KeyPointsPairMatcher {

  public static final int IF_MIN_NOT_FOUND = 0;
  private Image first;
  private Image second;

  public KeyPointsPairMatcher(Image first, Image second) {
    this.first = first;
    this.second = second;
  }

  public List<KeyPoint[]> getCorePointsPairs() {
    KeyPoint[] firstImagePoints = first.getKeyPoints();
    KeyPoint[] secondImagePoints = second.getKeyPoints();

    setClosestNeighbours(firstImagePoints, secondImagePoints);
    setClosestNeighbours(secondImagePoints, firstImagePoints);

    return Arrays.stream(firstImagePoints)
        .filter(KeyPoint::hasMutualClosestNeighbour)
        .map(point -> new KeyPoint[]{point, point.getClosestNeighbour()})
        .collect(Collectors.toList());
  }

  private void setClosestNeighbours(KeyPoint[] neighboursSetFor, KeyPoint[] neighboursSetFrom) {
    Arrays.stream(neighboursSetFor)
        .forEach(point -> point.setClosestNeighbour(closerNeighbour(point, neighboursSetFrom)));
  }

  private KeyPoint closerNeighbour(KeyPoint point, KeyPoint[] secondImagePoints) {
    return Arrays.stream(secondImagePoints)
        .min(Comparator.comparingInt(point::featureDistance))
        .orElse(secondImagePoints[IF_MIN_NOT_FOUND]);
  }
}
