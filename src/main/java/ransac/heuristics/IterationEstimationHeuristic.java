package ransac.heuristics;

public class IterationEstimationHeuristic implements IterationsHeuristic {

  private double isGoodEstimationProbability;
  private double isNotNoiseProbability;
  private double samples;

  public IterationEstimationHeuristic(
      double isGoodEstimationProbability, double isNotNoiseProbability, double samples) {
    this.isGoodEstimationProbability = isGoodEstimationProbability;
    this.isNotNoiseProbability = isNotNoiseProbability;
    this.samples = samples;
  }

  @Override
  public int numberOfIterations() {
    return (int)
        (Math.log(1 - isGoodEstimationProbability)
            / Math.log(1 - Math.pow(isNotNoiseProbability, samples)));
  }
}
