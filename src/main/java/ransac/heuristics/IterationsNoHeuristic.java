package ransac.heuristics;

public class IterationsNoHeuristic implements IterationsHeuristic {

  private int iterations;

  public IterationsNoHeuristic(int iterations) {
    this.iterations = iterations;
  }

  @Override
  public int numberOfIterations() {
    return iterations;
  }
}
