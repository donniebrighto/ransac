package ransac.heuristics;

import java.util.List;
import java.util.Random;
import ransac.KeyPoint;

public interface SamplesDrawingHeuristic {

  Random random = new Random();

  List<KeyPoint[]> drawPairs(List<KeyPoint[]> data, int samples);

  default int[] distinctRandoms(List<KeyPoint[]> data, int samples) {
    if (data.isEmpty()) {
      return null;
    }
    int[] distinctRandoms = new int[samples];
    for (int i = 0; i < distinctRandoms.length; i++) {
      int rand = random.nextInt(data.size());
      for (int j = i; j >= 0; j--) {
        if (distinctRandoms[j] == rand) {
          i--;
          break;
        }
        if (j == 0) {
          distinctRandoms[i] = rand;
        }
      }
    }
    return distinctRandoms;
  }

  void incrementCounter(KeyPoint[] pair);
}
