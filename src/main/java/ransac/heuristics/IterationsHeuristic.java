package ransac.heuristics;

public interface IterationsHeuristic {

  int numberOfIterations();

}
