package ransac.heuristics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import ransac.KeyPoint;

public class SamplesDrawingDistanceHeuristic implements SamplesDrawingHeuristic {

  // r^2 < (x1 - x2)^2 + (y1 - y2)^2 < R^2
  private double r;
  private double R;

  public SamplesDrawingDistanceHeuristic(double r, double R) {
    this.r = r;
    this.R = R;
  }

  public static int average = 0;

  public List<KeyPoint[]> drawPairs(List<KeyPoint[]> data, int samples) {
    KeyPoint[] firstPair = data.get(random.nextInt(data.size()));
    List<KeyPoint[]> inRange = data.stream().filter(pair ->
        isInRange(pair, firstPair)
    ).collect(Collectors.toCollection(ArrayList::new));
    inRange.add(firstPair);
    if (inRange.size() < samples) {
      average++;
    }
    for (int i = inRange.size(); i < samples; i++) {
      inRange.add(data.get(random.nextInt(data.size())));
    }
    return inRange.subList(0, samples);
  }

  @Override
  public void incrementCounter(KeyPoint[] pair) {

  }

  private boolean isInRange(KeyPoint[] pair, KeyPoint[] firstPair) {
    double firstImageDistance =
        Math.pow((firstPair[0].x - pair[0].x), 2) + Math.pow((firstPair[0].y - pair[0].y), 2);
    double secondImageDistance =
        Math.pow((firstPair[1].x - pair[1].x), 2) + Math.pow((firstPair[1].y - pair[1].y), 2);
    return Math.pow(r, 2) < firstImageDistance && firstImageDistance < Math.pow(R, 2) && Math.pow(r, 2) < secondImageDistance && secondImageDistance < Math.pow(R, 2);
  }


}
