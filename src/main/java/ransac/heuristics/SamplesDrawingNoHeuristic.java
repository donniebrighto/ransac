package ransac.heuristics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import ransac.KeyPoint;

public class SamplesDrawingNoHeuristic implements SamplesDrawingHeuristic {

  public List<KeyPoint[]> drawPairs(List<KeyPoint[]> data, int samples) {
    int[] distinctRandoms = distinctRandoms(data, samples);
    return Arrays.stream(distinctRandoms)
        .mapToObj(rand -> data.get(rand))
        .collect(Collectors.toCollection(ArrayList::new));
  }

  @Override
  public void incrementCounter(KeyPoint[] pair) {

  }

}
