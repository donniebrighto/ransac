package ransac.heuristics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ransac.KeyPoint;

public class SamplesDrawingScoreProbabilityHeuristic implements SamplesDrawingHeuristic {

  public static Map<KeyPoint[], Integer> scoreCounter;

  public SamplesDrawingScoreProbabilityHeuristic(List<KeyPoint[]> data) {
    scoreCounter = new HashMap<>();
    data.forEach(pair -> scoreCounter.put(pair, 1));
  }

  @Override
  public List<KeyPoint[]> drawPairs(List<KeyPoint[]> data, int samples) {
    List<KeyPoint[]> result = new ArrayList<>();
    while (result.size() < samples) {
      for (KeyPoint[] pair : data) {
        if (random.nextDouble() < (scoreCounter.get(pair) / (double)data.size())) {
          result.add(pair);
        }
        if (result.size() == samples) {
          break;
        }
      }
    }
    return result;
  }

  @Override
  public void incrementCounter(KeyPoint[] pair) {
    Integer counter = scoreCounter.get(pair);
    scoreCounter.put(pair, counter + 1);
  }

  private double random(List<KeyPoint[]> data) {
    return random.nextDouble() / (data.size() / 10.0);
  }
}
