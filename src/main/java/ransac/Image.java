package ransac;

import java.util.Arrays;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class Image {

  private String name;
  private KeyPoint[] keyPoints;

  public Image(String name, KeyPoint[] keyPoints) {
    this.name = name;
    this.keyPoints = keyPoints;
    Arrays.stream(keyPoints).forEach(point -> point.setImage(this));
  }

  public void transform(Mat model) {
    Arrays.stream(keyPoints)
        .forEach(
            point -> {
              Mat mat1 = new Mat(3, 1, CvType.CV_64F);
              mat1.put(0, 0, point.getxCoord());
              mat1.put(1, 0, point.getyCoord());
              mat1.put(2, 0, 1);

              Mat result = Mat.zeros(3, 1, CvType.CV_64F);
              Core.gemm(model, mat1, 1, new Mat(), 0, result, 0);

              double x = result.get(0, 0)[0];
              double y = result.get(1, 0)[0];
              point.setCoord(x, y);
            });
  }

  public String getName() {
    return name;
  }

  public KeyPoint[] getKeyPoints() {
    return keyPoints;
  }

  @Override
  public String toString() {
    return "Image{" +
        "name='" + name + '\'' +
        ", keyPoints=" + Arrays.toString(keyPoints) +
        '}';
  }
}
