import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import ransac.*;
import ransac.heuristics.IterationEstimationHeuristic;
import ransac.heuristics.IterationsNoHeuristic;
import ransac.heuristics.SamplesDrawingDistanceHeuristic;
import ransac.heuristics.SamplesDrawingNoHeuristic;
import ransac.heuristics.SamplesDrawingScoreProbabilityHeuristic;

public class Main {

  // Compulsory
  static {
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
  }

  private static final String FIRST_IMAGE_FEATURES = "balagan.png.haraff.sift";
  private static final String SECOND_IMAGE_FEATURES = "biurko.png.haraff.sift";

  public static void main(String[] args) {

    Image balagan = Loader.loadImage(FIRST_IMAGE_FEATURES);
    Image biurko = Loader.loadImage(SECOND_IMAGE_FEATURES);

    KeyPointsPairMatcher matcher = new KeyPointsPairMatcher(balagan, biurko);
    List<KeyPoint[]> corePairs = matcher.getCorePointsPairs();
    CoherentPairsFilter coherentFilter = new CoherentPairsFilter();
    List<KeyPoint[]> coherentPairs = coherentFilter.coherentPairs(50, 30, corePairs);
    List<Ransac> ransacList = new ArrayList<>();
    Ransac ransacNo = new Ransac(new SamplesDrawingNoHeuristic(), new IterationsNoHeuristic(500), 20);
    Ransac ransacProb = new Ransac(new SamplesDrawingScoreProbabilityHeuristic(corePairs), new IterationsNoHeuristic(1000), 100);
    Ransac ransacIter = new Ransac(new SamplesDrawingNoHeuristic(), new IterationEstimationHeuristic(0.5, 0.1, 3), 100);
    Ransac ransacDist = new Ransac(new SamplesDrawingDistanceHeuristic(Math.sqrt(Math.pow(800, 2) + Math.pow(600,2)) * 0.01, Math.sqrt(Math.pow(800, 2) + Math.pow(600,2)) * 0.3), new IterationsNoHeuristic(1000), 100);

    ransacList.add(ransacNo);
    ransacList.add(ransacProb);
    ransacList.add(ransacIter);
    ransacList.add(ransacDist);

    Model model = ransacNo.estimateModel(corePairs, 3);
    Drawer drawer = new Drawer("balagan.png", "biurko.png");
    drawer.draw(corePairs, Color.ORANGE);
    drawer.draw(model.getConsensus(), Color.GREEN);
    drawer.saveToFile("test.png");

    Model model1 = ransacNo.estimateModel(corePairs, 4);
    Drawer drawer1 = new Drawer("balagan.png", "biurko.png");
    drawer1.draw(corePairs, Color.ORANGE);
    drawer1.draw(model1.getConsensus(), Color.RED);
    drawer1.saveToFile("test1.png");

//    Mat src = Imgcodecs.imread("src\\main\\resources\\balagan.png");
//    if (src.empty()) {
//      System.err.println("Cannot read image: ");
//      System.exit(0);
//    }
//
//    Mat warpDst = Mat.zeros(src.rows(), src.cols(), src.type());
//    Imgproc.warpPerspective(src, warpDst, model.getTransformation(), warpDst.size());
//
//    Imgcodecs.imwrite("src\\main\\resources\\balagan_warp.png", warpDst);

//    Drawer drawer = new Drawer("balagan.png", "biurko.png");
//    drawer.draw(corePairs, Color.ORANGE);
//    drawer.draw(coherentPairs, Color.GREEN);
//    drawer.saveToFile("pairs.png");



//    System.out.println(corePairs.size());
//    for (int err = 5; err < 250; err += 20) {
//      Ransac ransac = new Ransac(new SamplesDrawingNoHeuristic(), new IterationsNoHeuristic(1000), err);
//      Model model = ransac.estimateModel(corePairs, 3);
//      Drawer drawer = new Drawer("balagan.png", "biurko.png");
//      drawer.draw(corePairs, Color.ORANGE);
//      drawer.draw(model.getConsensus(), Color.RED);
//      drawer.saveToFile("pairsErr" + err + ".png");
//      System.out.println("Err: " + err + " Cons: " + model.getConsensus().size());
//    }

//    int heur = 0;
//    for(Ransac ransac : ransacList) {
//      Model model = ransac.estimateModel(corePairs, 3);
//      Drawer drawer = new Drawer("balagan.png", "biurko.png");
//      drawer.draw(corePairs, Color.ORANGE);
//      drawer.draw(model.getConsensus(), Color.RED);
//      System.out.println("Heur: " + heur + " Cons: " + model.getConsensus().size());
//      drawer.saveToFile("pairsHeur" + heur++ + ".png");
//    }

//    balagan.transform(model.getTransformation());
//    drawer = new Drawer("balagan_warp.png", "biurko.png");
//    drawer.draw(corePairs, Color.ORANGE);
//    drawer.saveToFile("pairs_warp.png");
  }
}
